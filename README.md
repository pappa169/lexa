This is a sample C++17 program, just a demo.
It counts total, unique, longest words and top 10 used words from a file specified at the command line.
If the command line argument is a directory, then it iterates recursively through all files and displays the stats for every file.
At the end a global statistic is also displayed.
The program can handle normal and multi character strings.
