#include "Invoker.h"
#include <iostream>

int main (int argc, char *argv[])
{
	if(argc != 2)
	{
		std::cerr << "Invalid number of arguments!" << std::endl;
		std::cerr << "Usage: lexa FILE" << std::endl;
		return 1;
	}
	Invoker<char> invoker(std::cout);
	invoker.run(argv[1]);
	
	return 0;
}
