#include "LexicalAnalyzer.h"
#include <filesystem>

namespace fs = std::filesystem;

///\brief Wrapper class to call the LexicalAnalyzer on every file.
///It has a run method with the command line argument. Here will be iterated over all possible files in a recursive way.
///Every file will generate it's own statistic, which will be written to the output stream given to the constructor.
///There is also a global statistic, it's results will be printed at the end, invoked from the destructor.
template<typename charT>
class Invoker
{
public:
  Invoker(std::basic_ostream<charT>& outStream)
  : mrOutStream(outStream) {};
  ~Invoker()
  {
    mrOutStream << "Global statistics: " << std::endl;
    mGlobalLexicalAnalyzer.FormatResult(mrOutStream);
  };
  Invoker(const Invoker&) = delete;
  Invoker(const Invoker&&) = delete;
  Invoker& operator = (const Invoker&) = delete;
  //----------------------------------------------------------------------------------------------------------
  void run(const fs::path& fileSystemPath)
  //----------------------------------------------------------------------------------------------------------
  {
    if(fs::is_regular_file(fileSystemPath))
		{
      mrOutStream << "Processing file: " << fileSystemPath << std::endl;
      processFile(fileSystemPath);
      mrOutStream << "-------------------------------------------------------------------------" << std::endl;
    }
    else if(fs::is_directory(fileSystemPath))
    {
      for (const auto& entry : fs::directory_iterator(fileSystemPath))
        run(entry);
    }
  }

private:
  //----------------------------------------------------------------------------------------------------------
  void processFile(const fs::path& fileSystemPath)
  //----------------------------------------------------------------------------------------------------------
  {
		LexicalAnalyzer<charT> fileAnalyzer;
		std::basic_ifstream<charT> stream;
		stream.open(fileSystemPath);
		if(stream)
		{
			fileAnalyzer.Process(stream);
			fileAnalyzer.FormatResult(mrOutStream);
			stream.close();
      stream.open(fileSystemPath);
      mGlobalLexicalAnalyzer.Process(stream);
      stream.close();
		}
		else
		{
			mrOutStream << "Could not open file:" << fileSystemPath << std::endl;
		}
  }
private:
  LexicalAnalyzer<charT> mGlobalLexicalAnalyzer;
  std::basic_ostream<charT>& mrOutStream;
};
