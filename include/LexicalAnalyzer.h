#include "IProcessor.h"
#include <string>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <map>

///\brief Structure where to save the statistics
template <typename charT>
struct Result
{
  std::size_t mTotalWords {0};
  std::size_t mUniqueWords {0};
  std::vector<std::basic_string<charT>> mLongestWords;
};

///\brief Class that analizes lexically a stream, saves statistics to the Result struct defined above
///It counts all, unique, longest and 10 most used words
///Implements the IProcessor interface
template <typename charT>
class LexicalAnalyzer : public IProcessor<charT, Result<charT>>
{
private:
  using stringT = std::basic_string<charT>;
  using stringFreqT = std::pair<stringT, std::size_t>;
public:
  //----------------------------------------------------------------------------
  LexicalAnalyzer()
  //----------------------------------------------------------------------------
  {
  }

  virtual ~LexicalAnalyzer()
  {
  }

  //----------------------------------------------------------------------------
  void Process(std::basic_ifstream<charT>& stream) override
  //----------------------------------------------------------------------------
  {
    //loop through the stream by word, insert word and incremented word frequency in the map
    std::transform(std::istream_iterator<stringT, charT>(stream),
                   std::istream_iterator<stringT, charT>(),
                   std::inserter(mWordMap, mWordMap.end()),
                   [this](const auto& string) -> stringFreqT
                   { return std::pair(string, mWordMap[string]++); } );
  }

  ///\brief Format the results and writes to the \param outStream
  //----------------------------------------------------------------------------
  void FormatResult(std::basic_ostream<charT>& outStream) override
  //----------------------------------------------------------------------------
  {
      outStream << "Total words:" << GetTotalWords() << std::endl;
      outStream << "Unique words:" << GetNrUniqueWords() << std::endl;
      if(GetTotalWords() > 0)
      {
        outStream << "Longest words,";
        const auto& longestWordsVector { GetLongestWords() };
        outStream << "Size: " << longestWordsVector[0].length() <<  std::endl;
        for(auto word : longestWordsVector)
        {
          outStream << word << "  ";
        }
        outStream << std::endl;

        outStream << "Top 10 words(more than 3 chr): " << std::endl;
        for(const auto& pair : GetTop(10, 3))
        {
          outStream << pair.first << "  "  << pair.second << std::endl;
        }
      }
  }

private:
  ///\brief Creates the top of the most used words longer then \param wordsize
  ///\param topSize How many words should be in the top
  //----------------------------------------------------------------------------
  std::vector<stringFreqT> GetTop(const std::size_t topSize, const std::size_t wordSize) const
  //----------------------------------------------------------------------------
  {
    std::vector<stringFreqT> vec;
    if(mWordMap.size() == 0)
      return vec;
    //copy words longer as wordSize from map to the vector
    std::copy_if(mWordMap.begin(),
              mWordMap.end(),
              std::back_inserter(vec),
              [wordSize](const auto& pair)
              {
                return pair.first.length() >= wordSize;
              });
    //sort the vector by words frequency
    std::sort(vec.begin(), vec.end(),
              [](const stringFreqT& v1, const stringFreqT& v2)
              {
                if(v1.second != v2.second)
                  return v1.second > v2.second;
                return v1.first < v2.first;//if the frequency is equal use alphabetic order of words
              });

    auto toKeep = std::min(vec.size(), topSize);
    vec.erase(vec.begin() + toKeep, vec.end());//keep the first toKeep elements, erase the others
    return vec;
  }

  ///\brief Number of unique words
  //----------------------------------------------------------------------------
  std::size_t GetNrUniqueWords() const
  //----------------------------------------------------------------------------
  {
    return mWordMap.size();
  }

  ///\brief Number of total words
  //----------------------------------------------------------------------------
  std::size_t GetTotalWords() const
  //----------------------------------------------------------------------------
  {
    std::size_t words {0};
    for(const auto& pair : mWordMap)
    {
        words += pair.second;
    }
    return words;
  }

  ///\brief Returns a vector with the longest words
  //----------------------------------------------------------------------------
  std::vector<stringT> GetLongestWords() const
  //----------------------------------------------------------------------------
  {
    std::vector<stringT> longestStrings;
    //find first longest string
    auto max = std::max_element(mWordMap.begin(), mWordMap.end(),
                    [](const auto& w1, const auto& w2)
                    {
                      return w1.first.length() < w2.first.length();
                    });
    if(max != mWordMap.end())
    {
      longestStrings.emplace_back(max->first);
      //find the other strings with the same length
      auto it {max};
      it++;
      while((it = std::find_if(it, mWordMap.end(),
                                  [max](const auto& pair)
                                {
                                  return pair.first.length() == max->first.length();
                                })) !=  mWordMap.end())
        {
          longestStrings.emplace_back(it->first);
          it++;
        }
    }

    return longestStrings;
  }
private:
  ///\brief map containing unique words with occurance number(frequency)
  std::map<std::basic_string<charT>, std::size_t> mWordMap;
};
