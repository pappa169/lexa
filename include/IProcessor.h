#include <fstream>

///\brief Interface class to define operations. 
/// Basicaly an input stream will be processed and the result will be saved in the ResultT struct and also written to an output stream.
///\tparam charT character type
///\tparam ResultT struct used to store the results into
template <typename charT, typename ResultT>
class IProcessor
{
public:
  ///\brief Input stream processing function
  virtual void Process(std::basic_ifstream<charT>& inStream) = 0;
  ///\brief Function to format and write results to the output stream
  virtual void FormatResult(std::basic_ostream<charT>& outStream) = 0;
  ///\brief Getter for the results.
  const ResultT& GetResult() const
  {
    return mResult;
  }
protected:
  ResultT mResult;
};
